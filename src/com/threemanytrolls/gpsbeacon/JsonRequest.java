package com.threemanytrolls.gpsbeacon;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.UnknownServiceException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class JsonRequest extends AsyncTask<JSONObject, Void, JSONObject> {
	
	private static String baseUrl = "https://eeppinen.no-ip.org/gps-beacon/";
	private static String loginService = "wip_class.php";
	
	
	// always verify the host - dont check for certificate
	final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};
	
	@Override
	protected JSONObject doInBackground(JSONObject... params) {
		try {
			
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			// From https://www.washington.edu/itconnect/security/ca/load-der.crt
			//InputStream caInput = new BufferedInputStream(new FileInputStream(System.getProperty("user.dir")+"public.crt"));
			String str = "-----BEGIN CERTIFICATE-----\n"+
"MIIDJzCCAg+gAwIBAgIJAJ6TWpk1LM3kMA0GCSqGSIb3DQEBBQUAMCoxCzAJBgNV\n"+
"BAYTAkZJMRswGQYDVQQDDBJlZXBwaW5lbi5uby1pcC5vcmcwHhcNMTQwOTI1MTcw\n"+
"MDI1WhcNMTUwOTI1MTcwMDI1WjAqMQswCQYDVQQGEwJGSTEbMBkGA1UEAwwSZWVw\n"+
"cGluZW4ubm8taXAub3JnMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA\n"+
"mh/MCmf5DowDvh5Y3usdXzQwDumtOqbRhbeZbRymfCSli/qrX6nc4aWibWFlXoK2\n"+
"7jG3O1GrNkCX0dsM9fIO2RnVIWXMpA/alIhaP+wznNt063D2/oYnoVJRJWm9t8IQ\n"+
"yml6qEisuBqOHZUFGV8gGs5QjFA6hRFXwY7G4HPMxSn+uQSbJGgWhefnSb7m34H/\n"+
"HCjeyZnMjMQRyqoncCfk4lZftxMpjCEGMpTqBZehs6l4WgwQ4OYXp7FBCb9BOzEh\n"+
"wfsRGYqam9Hni7Xlp+/3rXoLRuVaTxpjn1V0np2pGM9nAsXbOIQccPh2wxQbHb+z\n"+
"nLG9xKDd2zPqWTML4chYPwIDAQABo1AwTjAdBgNVHQ4EFgQUd5bXuaKL7QSle96z\n"+
"nZXRdWUV+e4wHwYDVR0jBBgwFoAUd5bXuaKL7QSle96znZXRdWUV+e4wDAYDVR0T\n"+
"BAUwAwEB/zANBgkqhkiG9w0BAQUFAAOCAQEAeFQOw/ccW1Hw7ySyADz7NUGJqcsR\n"+
"ZgMkh32DwRhvkwHg/Ssgiz742Bei83HXQeGKY/twukfUxkNz79Y+rnfI34k3v+dm\n"+
"nnUEDdghikiM/Sp6xfjHryL11lMCo3V0GFIixBwm6++RZH67eHQ8HA5GbNGLy46A\n"+
"x5BM6ZkM2WmsT7wWn8eQeOshdA5vfZqIjQ1LptHExJa4UB1x+dPfBhoGEn7vYUDy\n"+
"LFGFobLiR8uIjV/4Rh7apMXthg25+hhgON0iU19HViorr8lL1K/gL4RM7chiZDU2\n"+
"KAzqk8tavp0nqVmiK5JpAtIRreGKMUozzdxtBIq4lSLPxaTFXyYDgEpICw==\n"+
"-----END CERTIFICATE-----\n";
			InputStream caInput = new ByteArrayInputStream(str.getBytes());
			Certificate ca;
			try {
			    ca = cf.generateCertificate(caInput);
			    System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
			} finally {
			    caInput.close();
			}
			
			// Create a KeyStore containing our trusted CAs
			String keyStoreType = KeyStore.getDefaultType();
			KeyStore keyStore = KeyStore.getInstance(keyStoreType);
			keyStore.load(null, null);
			keyStore.setCertificateEntry("ca", ca);

			// Create a TrustManager that trusts the CAs in our KeyStore
			String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
			tmf.init(keyStore);
			
			// Create an SSLContext that uses our TrustManager
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, tmf.getTrustManagers(), null);

			URL url = new URL(baseUrl + loginService);
			
			HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			con.setSSLSocketFactory(context.getSocketFactory());
			
			JSONObject req = params[0];
			
			con.setDoOutput(true);

			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write( "request=" + URLEncoder.encode(req.toString(), "UTF-8") );
            wr.flush();
			
            
            
			InputStream istream = con.getInputStream();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(istream));
			
			StringBuilder sb = new StringBuilder();
			String line = null;
			
			while((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line);
			}
			
			JSONObject json = new JSONObject(sb.toString());
			
			reader.close();
			istream.close();
			con.disconnect();
			
			return json;
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(UnknownServiceException e) {
				e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	protected void onPostExecute(JSONObject result) {
        //showDialog("Downloaded " + result + " bytes");
    }
}
