package com.threemanytrolls.gpsbeacon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.threemanytrolls.gpsbeacon.models.User;

import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends ActionBarActivity {
	Button button;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		final LoginActivity self = this;
		
		TextView registerLabel = (TextView)findViewById(R.id.register_label);
		registerLabel.setText(Html.fromHtml("No account yet? Click <font color=blue>here</font> to create one."));
		
		registerLabel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(self, RegisterActivity.class);
				startActivity(intent);
				
			}
			
		});
		
		button = (Button)findViewById(R.id.loginButton);
		
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				User user = User.getInstance();
				
				EditText username = (EditText)(findViewById(R.id.usernameInput));
				EditText password = (EditText)(findViewById(R.id.passwordInput));
				
				int status = user.logIn(username.getText().toString(), password.getText().toString());
				
				if(status == User.SUCCESS) {
					Toast.makeText(getApplicationContext(), "Welcome, " + user.getUsername(), Toast.LENGTH_SHORT).show();
					
					Intent intent = new Intent(self, HomeActivity.class);
					startActivity(intent);
					finish();
					
				}
				
				else {
					Toast.makeText(getApplicationContext(), "Login failed :(", Toast.LENGTH_SHORT).show();
				}

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
