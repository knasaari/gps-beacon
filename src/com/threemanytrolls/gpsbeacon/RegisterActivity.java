package com.threemanytrolls.gpsbeacon;

import com.threemanytrolls.gpsbeacon.models.User;

import android.support.v4.content.IntentCompat;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		final RegisterActivity self = this;
		
		Button registerBtn = (Button)findViewById(R.id.register_button);
		
		registerBtn.setOnClickListener(new OnClickListener() {
		
			@Override
			public void onClick(View arg0) {
				EditText username = (EditText)(findViewById(R.id.username));
				EditText password = (EditText)(findViewById(R.id.password));
				EditText passwordAgain = (EditText)(findViewById(R.id.password_again));
				
				User user = User.getInstance();
				int status = user.register(username.getText().toString(), password.getText().toString(), passwordAgain.getText().toString());
				
				if(status == User.SUCCESS) {
					Toast.makeText(getApplicationContext(), "Account created. You may login now.", Toast.LENGTH_LONG).show();
					
					//Intent intent = new Intent(self, HomeActivity.class);
					//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
					//startActivity(intent);
					finish();
				}
				
				else {
					Toast.makeText(getApplicationContext(), "Uh oh, something went wrong.", Toast.LENGTH_SHORT).show();
				}
				
			}});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
