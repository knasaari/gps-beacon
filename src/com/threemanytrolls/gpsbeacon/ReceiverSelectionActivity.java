package com.threemanytrolls.gpsbeacon;

import java.util.List;

import com.threemanytrolls.gpsbeacon.models.Message;
import com.threemanytrolls.gpsbeacon.models.User;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ReceiverSelectionActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_receiver_selection);
		
		
		
		ListView lv = (ListView)findViewById(R.id.userlist_view);
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2,
					long arg3) {
				// Acquire a reference to the system Location Manager
				final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

				// Define a listener that responds to location updates
				LocationListener locationListener = new LocationListener() {
				    public void onLocationChanged(Location location) {
				      // Called when a new location is found by the network location provider.
				      //makeUseOfNewLocation(location);
				    	
				    	locationManager.removeUpdates(this);
				    	
				    	User user = User.getInstance();
						user.sendMessageTo(user.getFriends().get(arg2), location.getLatitude(), location.getLongitude(), location.getAltitude());
						
						Toast.makeText(getApplicationContext(), "Mesage sent!", Toast.LENGTH_SHORT).show();
						finish();
				    	
				    }

					@Override
					public void onProviderDisabled(String arg0) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onProviderEnabled(String arg0) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onStatusChanged(String arg0, int arg1,
							Bundle arg2) {
						// TODO Auto-generated method stub
						
					}

				  };

				  try {
				// Register the listener with the Location Manager to receive location updates
					  if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
						  locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 1, locationListener);
					  } else {
						  Log.d("foobar", "No provider! :(");
						  Toast.makeText(getApplicationContext(), "No location provider available!", Toast.LENGTH_LONG).show();
					  }

				  } catch (Exception e) {
					  e.printStackTrace();
				  }
				//locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, locationListener, null);
				
				
			}
		});
		
		User u = User.getInstance();
		
		List<String> list = u.getFriends();
		
		ArrayAdapter<String> aa = new ArrayAdapter<String>(this,  R.layout.messages_list_item, list);
		lv.setAdapter(aa);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.receiver_selection, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
}
