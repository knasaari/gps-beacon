package com.threemanytrolls.gpsbeacon;

import java.util.ArrayList;
import java.util.List;

import com.threemanytrolls.gpsbeacon.HomeActivity.PlaceholderFragment;
import com.threemanytrolls.gpsbeacon.models.Message;
import com.threemanytrolls.gpsbeacon.models.User;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class MessagesFragment extends Fragment {
	private static MessagesFragment instance;
	
	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_messages, container,
				false);
		
		ListView lv = (ListView)rootView.findViewById(R.id.messagelist_view);
		
		User u = User.getInstance();
		
		final List<Message> list = u.getMessages();
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				Toast.makeText(getActivity(), "This message is sent from: " + list.get(arg2).locationToString(), Toast.LENGTH_SHORT).show();
				//finish();
			}
		});
		
		
		
		ArrayAdapter<Message> aa = new ArrayAdapter<Message>(rootView.getContext(),  R.layout.messages_list_item, list);
		lv.setAdapter(aa);
		
		return rootView;
	}
	
	public static MessagesFragment getInstance() {
		if(instance == null) {
			instance = new MessagesFragment();
			Bundle args = new Bundle();
			instance.setArguments(args);
		}
		return instance;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((HomeActivity) activity).onSectionAttached(3);
	}
}
