package com.threemanytrolls.gpsbeacon.models;

/**
 * TextMessage is a short text that be bundled with
 * location message.
 * 
 * @author jari
 *
 */
public class TextMessage extends MessageData {
	// constants
	private final short MAX_LENGHT = 160;
	
	// public fields
	// ...
	
	// private fields
	private String value = "";
	
	/**
	 * Creates a new message with text from first argument.
	 * @param message
	 */
	public TextMessage(final String message) {
		value = message;
		
		// Check for too long messages
		if (value.length() > MAX_LENGHT) {
			value = value.substring(0, MAX_LENGHT-3) + "...";
		}
	}
	
	public String getText() {
		return value;
	}
	
	@Override
	public String toString() {
		return "Text: " + value;
	}
}
