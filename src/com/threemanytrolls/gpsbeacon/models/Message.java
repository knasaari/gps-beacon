package com.threemanytrolls.gpsbeacon.models;

import java.util.Observable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Message is a container class for three types of messages: Text, Picture and Location.
 * 
 * @author Jari Saaranen
 *
 */
public class Message extends Observable {
	// Group receivers;
	// User from;
	
	private double lat = 0.0f;
	private double lon = 0.0f; 
	private double alt = 0.0f;
	
	private TextMessage textMessage;
	private boolean unread = true;
	
	public Message() {
		
	}
	
	public Message(JSONObject messageObject) {
		try {
			textMessage = new TextMessage((String)messageObject.get("text"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setTextMessage(TextMessage message) {
		textMessage = message;
	}
	
	public void unsetTextMessage() {
		textMessage = null;
	}
	
	public void read() {
		unread = false;
	}
	
	public boolean getIsUnread() {
		return unread;
	}
	
	@Override
	public String toString() {
		return textMessage.toString();
	}

	public void setLocation(double lat2, double lon2, double alt2) {
		this.lat = lat2;
		this.lon = lon2;
		this.alt = alt2;
	}
	
	public String locationToString() {
		return "latitude: " + lat + " longitude: " + lon + " altitude: " + alt;
	}
}
