package com.threemanytrolls.gpsbeacon.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.threemanytrolls.gpsbeacon.JsonRequest;

import android.app.Application;
import android.content.Context;
import android.util.Log;

/**
 * 
 * @author Jari Saaranen
 *
 */
public class User extends Observable{
	// status codes for different situations
	public static final int SUCCESS = 0;
	public static final int FAIL = 1;
	public static final int LOGIN_NO_USER = 2;
	public static final int NO_MATCH = 3;
	
	public static final String LOGIN_ACTION_CONFIRMATION = "login-response";
	
	private static User instance;
	
	// actual fields of the user-model
	private String username;
	private String token;
	private boolean loggedIn;
	
	private ArrayList<Message> messages;
	
	private User() {
		username = "anon";
		loggedIn = false;
		token = "there is no spoon";
		messages = new ArrayList();
	}
	
	public boolean isLoggedIn() {
		return loggedIn;
	}
	
	public int logIn(String username, String password) {
		try {
			JsonRequest request = new JsonRequest();
			JSONObject reqObj = new JSONObject();
			
			reqObj.put("action", "login");
			reqObj.put("user", username);
			reqObj.put("pass", password);
			
			request.execute(reqObj);
			
			JSONObject response = request.get();
			
			if (response == null) {
				throw new Exception("No response");
			}
			
			boolean success = (Boolean)response.get("success");
			
			if(success) {
				JSONObject userObj = response.getJSONObject("user");
				
				this.loggedIn = true;
				this.username = (String)userObj.getString("name");
				this.token = (String)userObj.getString("token");
				
				Log.d("foobar", token);
				
				return SUCCESS;
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return FAIL;
	}
	
	public int register(String username, String password, String passwordAgain) {
		if(!password.equals(passwordAgain)) {
			return NO_MATCH;
		}
		
		try {
			JsonRequest request = new JsonRequest();
			JSONObject reqObj = new JSONObject();
			
			reqObj.put("action", "register");
			
			reqObj.put("user", username);
			reqObj.put("pass", password);
			reqObj.put("pass2", passwordAgain);
			
			request.execute(reqObj);
			JSONObject response = request.get();
			
			if (response == null) {
				throw new Exception("No response");
			}
			
			boolean success = (Boolean)response.get("success");
			
			if(success) {
				//JSONObject userObj = response.getJSONObject("user");
				
				//this.loggedIn = true;
				//this.username = (String)userObj.getString("name");
				return SUCCESS;
			}
			
			Log.d("json", response.toString(3));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return FAIL;
	}
	
	/**
	 * Full status update. User gets all information he could
	 * ever need while using the app.
	 * 
	 * That means messages and friend list.
	 * @return
	 */
	public int getAll() {
		JsonRequest request = new JsonRequest();
		JSONObject reqObj = new JSONObject();
		
		try {
			reqObj.put("action", "get-all");
			reqObj.put("token", token);
			
			request.execute(reqObj);
			JSONObject response = request.get();
			
			if(response == null) {
				throw new Exception("No response");
			}
			
			if(response.getBoolean("success") == false) {
				throw new Exception("request failed");
			}
			
			JSONArray messageArray = response.getJSONArray("messages");
			
			// If there are messages waiting
			for(int i = 0; i < messageArray.length(); i++) {
				
				Message m = new Message();
			
				JSONObject mO = (JSONObject) messageArray.get(i);
			
				m.setTextMessage(
						new TextMessage(
								mO.getString("text")
								));
				
				JSONObject location = mO.getJSONObject("location");
				
				double lat = location.getDouble("latitude");
				double lon = location.getDouble("longitude");
				double alt = location.getDouble("altitude");
				
				m.setLocation(lat, lon, alt);
				
				messages.add(m);
			}
			
			return SUCCESS;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return FAIL;
	}
	
	public ArrayList<String> getFriends() {
		JsonRequest request = new JsonRequest();
		JSONObject reqObj = new JSONObject();
		
		try {
			reqObj.put("action", "get-users");
			reqObj.put("token", token);
			
			request.execute(reqObj);
			JSONObject response = request.get();
			
			if(response == null) {
				throw new Exception("No response");
			}
			
			if(response.getBoolean("success") == false) {
				throw new Exception("request failed");
			}
			
			JSONArray userArray = response.getJSONArray("users");
			ArrayList<String> usernames = new ArrayList<String>();
			
			for(int i = 0; i < userArray.length(); i++) {
				usernames.add(userArray.getString(i));
			}
			
			return usernames;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}
	
	public String getUsername() {
		return username;
	}
	
	public int getMessageCount(boolean unreadOnly) {
		if(!unreadOnly) {
			return messages.size();
		}
		
		int count = 0;
		for(Message m : messages) {
			if(m.getIsUnread()) {
				count++;
			}
		}
		
		return count;
	}
	
	public static User getInstance() {
		if(instance == null) {
			instance = new User();
		}
		
		return instance;
	}
	
	public ArrayList<Message> getMessages() {
		return this.messages;
	}
	
	public int sendMessageTo(String username, double lat, double lon, double alt) {
		JsonRequest request = new JsonRequest();
		JSONObject reqObj = new JSONObject();
		
		try {
			reqObj.put("action", "send-message");
			reqObj.put("token", token);
			reqObj.put("receivers", new JSONArray("["+username+"]"));
			
			JSONObject message = new JSONObject();
			
			JSONObject location = new JSONObject();
			location.put("latitude", lat);
			location.put("longitude", lon);
			location.put("altitude", alt);
			
			message.put("location", location);
			message.put("text", "Hi! Look where I am now!");
			reqObj.put("message", message);

			
			request.execute(reqObj);
			JSONObject response = request.get();
			
			if(response == null) {
				throw new Exception("No response");
			}
			
			if(response.getBoolean("success") == false) {
				throw new Exception("request failed");
			}
			
			//JSONArray messageArray = response.getJSONArray("messages");
			
			return SUCCESS;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return FAIL;
	}
}
