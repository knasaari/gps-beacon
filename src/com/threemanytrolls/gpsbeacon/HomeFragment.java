package com.threemanytrolls.gpsbeacon;

import java.util.List;

import com.threemanytrolls.gpsbeacon.models.Message;
import com.threemanytrolls.gpsbeacon.models.User;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class HomeFragment extends Fragment {
	private static HomeFragment instance;

	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_home, container,
				false);
		
		
		Button shareButton = (Button) rootView.findViewById(R.id.share_button);
		
		shareButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d("foobar", "clicked share");
				Intent intent = new Intent(rootView.getContext(), ReceiverSelectionActivity.class);
				rootView.getContext().startActivity(intent);
			}
		});
		
		return rootView;
	}
	
	public static HomeFragment getInstance() {
		if(instance == null) {
			instance = new HomeFragment();
			Bundle args = new Bundle();
			//instance.setArguments(args);
		}
		return instance;
	}
}
