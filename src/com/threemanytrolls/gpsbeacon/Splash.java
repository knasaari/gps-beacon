package com.threemanytrolls.gpsbeacon;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class Splash extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		Timer timer = new Timer();
		RunAppTask runAppTask = new RunAppTask();
		runAppTask.scope = this;
		
		timer.schedule(runAppTask, 1200);
	}
}

class RunAppTask extends TimerTask {
	public Activity scope;
	
	@Override
	public void run() {
		//TODO: Make decision where to go. Login or main
		
		Intent intent = new Intent(scope, LoginActivity.class);
		scope.startActivity(intent);
		scope.finish();
	}
	
}
